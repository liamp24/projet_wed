<?php
/*
  ./app/vues/template/index.php
  AFFICHE LES ZONES DYNAMIQUES
 */
 ?>
 <!doctype html>
 <html class="no-js" lang="zxx">
 <head>
   <?php include '../app/vues/template/partials/_head.php';?>
 </head>
 <body>
 <?php include '../app/vues/template/partials/_preloader.php'; ?>
 <?php include '../app/vues/template/partials/_header.php'; ?>
 <?php include '../app/vues/template/partials/_main.php'; ?>
 <?php include '../app/vues/template/partials/_footer.php'; ?>
 <?php include '../app/vues/template/partials/_scripts.php'; ?>
 </body>
 </html>
